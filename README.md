# tessais.gay

Fork of [swx](https://framagit.org/3hg/swx) for my personal website [tessais.gay](https://tessais.gay/).

## Additional dependencies:
- `tree` - for easy directory listing pages without Apache file listing pages
- `marked` - markdown parser
- `pup` - general purpose html processing tool, to be used for RSS/Atom support eventually

The markdown parser being used is called marked and it is configured to allow inline-HTML. However the site CSS does not necessarily support all of the available options. I also use pup for the upcoming rss feature.

## TODO:

- remove dependence on Apache web server features
- remove references to 'gnu3' (old site name)
- Convert generated /gnu3.static/articles/toParse.html to atom/rss
- add styling for `<code></code>` tags in CSS
