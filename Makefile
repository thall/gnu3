SOURCEDIR := gnu3
DESTDIR := gnu3.static
INDEXED := "img files"

all:
	sh ./swx $(SOURCEDIR)
	./swx_sitemap  $(DESTDIR) > $(DESTDIR)/sitemap.xml
	gzip --best -c $(DESTDIR)/sitemap.xml > $(DESTDIR)/sitemap.gz
	./swx_plan $(DESTDIR) > $(DESTDIR)/siteplan.html
	cp .roothtacc $(DESTDIR)/.htaccess
	./tree_index.sh $(DESTDIR)
	cp -R favicons/. $(DESTDIR)
	cp robots.txt ${DESTDIR}
	cat ./gnu3.static/articles/index.html | pup '#article' > ./gnu3.static/articles/toParse.html
	tail -n +2 ./gnu3.static/articles/toParse.html > ./gnu3.static/articles/toParse2.html
	head -n -0 ./gnu3.static/articles/toParse2.html > ./gnu3.static/articles/toGen.html
	echo '</html>' >> ./gnu3.static/articles/toGen.html
	sed -i '1i <html>' gnu3.static/articles/toGen.html


clean:
	rm -rf *.static
	rm -f swx.log
force:
	find $(SOURCEDIR) -exec touch {} \;
	make all
serve:
	sudo rm -rf /srv/http/*
	sudo cp -R gnu3.static/. /srv/http/
