# Gnu3 - Articles

Tessa Hall's long-form special interest and opinion blog.

# RSS
Subscribe via <a type="application/rss+xml" href="/articles/feed.xml">RSS</a> (not yet implemented)

# Recent posts:

<ul>
<li><a href="/articles/3.html" title="2021-3-21">The Meaning of GNU 3</a><li>
<li><a href="/articles/catan.html" title="2019-4-20">Different View of Catan</a><li>
<li><a href="/articles/iceland_pictures.html" title="2019-2-10">Pictures from Iceland</a><li>
<li><a href="/articles/new_website.html" title="2019-1-10">New Website!</a><li>
</ul>
