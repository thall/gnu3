## Pictures from my trip to Iceland

I have more, but since I am away from home right now I don't have access to them. I will add more later.

![glacier](/img/glacier.jpg)

![magma_chamber](/img/magma-chamber-from-inside.jpg)
