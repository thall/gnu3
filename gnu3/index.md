# tessais.gay

## Tessa Hall's personal webpage

My name is Tessa Hall (she/her). I am a nerd with a preference for ethical forms of computing with a Bachelor in Science in Computer Science from Trinity University in San Antonio, TX, USA. Currently a graduate student in Computer Science at DePaul University. I can enjoy programming recreationally, creating digital/photographic art, reading, and sometimes playing video games (but nothing too fast-paced). [Ethical Open Source](https://ethicalsource.dev/definition/) advocate. Currently working on research related to [KeYmaera X: An aXiomatic Tactical Theorem Prover for Hybrid Systems](https://keymaerax.org/) and pursing a masters in Computer Science.

### Recent Work

- Hall, T., & Mitsch, S. (2024). Counterexamples in CPS Theorem Proving. Greater Chicago Area Systems Research Workshop (GCASR), University of Illinois Chicago. 
  - [Zenodo](https://doi.org/10.5281/zenodo.14182062) ![DOI-Badge](https://zenodo.org/badge/DOI/10.5281/zenodo.14182062.svg)
  - [PDF](/files/publications/GCASR_Poster.pdf) [ZIP](/files/publications/GCASR_Poster.zip)

### Here are some personal links and contact info:

- <a href="https://framagit.org/thall" rel="me">My git</a> (what I primarily use)
- <a href="https://github.com/thall-gnu" rel="me">My GitHub</a> (I generally avoid GitHub these days. I don't like anything trying to be "social")
- My cell phone: Three4Six2For70ne6NineFour
- My email: tkhall(at)riseup(dot)net
- My [ssh public key](/files/id_rsa_thall.pub) Give me access to your servers! (Supervillian laughter)


### Other things on my site:

- [Blog](/articles/)
- [Pages](/docs/)


Here is a picture of my two kittens, Bit and Trit when I first got them:


![kittens](/img/kittens.png)

![https://geekfeminism.wikia.org/wiki/Richard_Stallman](/img/rms-is-bad.png)

![https://framagit.org/thall/emacs-conf](/img/created-emacs.jpg)

![https://anybrowser.org/campaign/](/img/anybrow.gif)
