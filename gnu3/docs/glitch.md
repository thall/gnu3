# Glitch Art and Digital Photography

Here is some of my glitch art. All the source images are also mine, but I use them purely as source materials for my glitch art. Everything here features a combination of pixelsorting, databending, traditional editing, and various other techniques (using free software). The website background is also one of my pieces.

![billiton](/img/glitch/billiton.jpg)
![corner](/img/glitch/corner.jpg)
![deletion](/img/glitch/deletion.jpg)
![leak](/img/glitch/leak.jpg)
![lightway](/img/glitch/lightway.jpg)
![poster](/img/glitch/poster.jpg)
![redeye](/img/glitch/redeye.jpg)
![shake](/img/glitch/shake.jpg)
![triplet](/img/glitch/triplet.jpg)
![zoom](/img/glitch/zoom.jpg)
![parallax](/img/glitch/parallax.jpg)
