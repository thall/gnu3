#!/bin/bash

# DESTDIR is $1

INDEXED="
				 img
				 files
				 "

for d in $INDEXED
do
		# technically untested with the makefile script, but I tested on prod and it worked once :P
		tree $1/$d/ -I index.html -i -h -H '' --hintro fileheader.html --houtro filefooter.html > $1/$d/index.html
done
exit


